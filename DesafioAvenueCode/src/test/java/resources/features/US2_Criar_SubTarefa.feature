#https://cucumber.io/docs/gherkin/reference/

@funcional
Feature: US2 - Criar subtarefa

		Como usuario do App ToDo
		Eu deveria ser capaz de criar uma subtarefa
		Entao eu posso dividir minhas tarefas em pedacos menores

		Criterios de aceitacao:
	
				O usuario deve ver um botao rotulado como "Manage Subtasks";
				Este botao deve ter o numero de subtarefas criadas para essas tarefas;
				Clicar neste botao abre uma caixa de dialogo modal:
						Este pop-up deve ter um campo somente leitura com o ID da tarefa e a descricao da tarefa;
						Deve haver um formulario para que voce possa inserir a <Descricao da Subtarefa> e data de vencimento da SubTask (formato MM / DD / AAAA);
						O usuario deve clicar no bot�o Adicionar para adicionar uma nova subtarefa;
						A descricao da tarefa e a data de vencimento sao campos obrigatorios;
						As subtarefas que foram adicionadas devem ser anexadas na parte inferior do dialogo;

Background: 
    Given acesso o Menu "My Tasks"

Scenario Outline: Incluir SubTask
    And a tarefa deve possuir <caracteres>
    And o usuario deve poder inserir uma nova tarefa acionando o botao Adicionar
    And adicionado, a tarefa deve ser anexada a lista de tarefas criadas
    And o usuario deve clicar em Manage SubTasks
    And Este botao deve ter o numero de subtarefas criadas para essas tarefas
    And o usuario deve incluir uma descricao da SubTasks <Descricao>
    When tambem deve incluir uma data de vencimento "MM/dd/yyyy"
    Then As subtarefas que foram adicionadas devem ser anexadas na parte inferior do dialogo

Examples:

 |caracteres| 				Descricao			   |
 |		50	  | "Descricao da SubTarefa" |
 |		50	  | 						""					 |