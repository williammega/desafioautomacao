
#https://cucumber.io/docs/gherkin/reference/

#Os bugs 5817, 5815 e 5810 nao foram automatizados por se tratar de bugs visuais ou de ajustes na especificao 

@bugs
Feature: Reproduzir os bugs encontrados nos testes das historias de usuario US1 e US2

		Como analista de automacao
		Eu deveria ser capaz de automatizar os bugs encontrados nas US1 e US2
		Entao eu posso reproduzir os bugs de forma automatica
		
Scenario Outline: Reproduzir de forma automatica o Bug5818
		Given acesso o Menu "My Tasks"
    And a tarefa deve ser vazia <Descricao>
    When o usuario deve poder inserir uma nova tarefa acionando o botao Adicionar
    Then adicionado, a tarefa com a descricao vazia deve ser anexada a lista de tarefas criadas

Examples:
 |Descricao|
 |		""   |
 
Scenario Outline: Reproduzir de forma automatica o Bug5811
		Given acesso o Menu "My Tasks"
    And a tarefa deve possuir <caracteres>
    And o usuario deve poder inserir uma nova tarefa acionando o botao Adicionar
    And adicionado, a tarefa deve ser anexada a lista de tarefas criadas
    And o usuario deve clicar em Manage SubTasks
    And este pop-up deve ter um campo somente leitura com o ID da tarefa e a descricao da tarefa
Examples:
 |caracteres|
 |		50	  |
    
Scenario Outline: Reproduzir de forma automatica o Bug5816
		Given acesso o Menu "My Tasks"
    And a tarefa deve possuir <caracteres>
    And o usuario deve poder inserir uma nova tarefa acionando o botao Adicionar
    And adicionado, a tarefa deve ser anexada a lista de tarefas criadas
    And o usuario deve clicar em Manage SubTasks
    And Este botao deve ter o numero de subtarefas criadas para essas tarefas
    And o usuario deve incluir uma descricao da SubTasks <Descricao>
    And tambem deve incluir uma data de vencimento "MM/dd/yyyy"
    When editar a subtarefa e confirma <Descricao>
    Then o sistema exibe a mensagem de erro "<!DOCTYPE html>"
    
Examples:
 |caracteres| 				Descricao			   |
 |		50	  | "Descricao da SubTarefa" |

Scenario: Reproduzir de forma automatica o Bug5808
		Given acesso o Menu "My Tasks"
    Then o sistema deve exibir a seguinte mensagem ", this is your todo list for today"

Scenario Outline: Reproduzir de forma automatica o Bug5812
    Given acesso o Menu "My Tasks"
    And a tarefa deve possuir <caracteres>
    And o usuario deve poder inserir uma nova tarefa acionando o botao Adicionar
    And adicionado, a tarefa deve ser anexada a lista de tarefas criadas
    And o usuario deve clicar em Manage SubTasks
    And Este botao deve ter o numero de subtarefas criadas para essas tarefas
    And o usuario deve incluir uma descricao da SubTasks <Descricao>
    When tambem deve incluir uma data de vencimento "dd/MM/yyyy"
    Then As subtarefas que foram adicionadas devem ser anexadas na parte inferior do dialogo
    
Examples:
 |caracteres| 				Descricao			   |
 |		50	  | "Descricao da SubTarefa" |

Scenario Outline: Reproduzir de forma automatica o Bug5813
    Given acesso o Menu "My Tasks"
    And a tarefa deve possuir <caracteres>
    And o usuario deve poder inserir uma nova tarefa acionando o botao Adicionar
    And adicionado, a tarefa deve ser anexada a lista de tarefas criadas
    And o usuario deve clicar em Manage SubTasks
    And Este botao deve ter o numero de subtarefas criadas para essas tarefas
    And o usuario deve incluir uma descricao da SubTasks <Descricao>
    When tambem deve incluir uma data de vencimento "MM/dd/yyyy"
    Then As subtarefas que foram adicionadas vazias devem ser anexadas na parte inferior do dialogo
    
Examples:
 |caracteres| 				Descricao			   |
 |		50	  | 					 ""						 |

Scenario Outline: Reproduzir de forma automatica o Bug5814
		Given acesso o Menu "My Tasks"
    And a tarefa deve possuir <caracteres>
    And o usuario deve poder inserir uma nova tarefa acionando o botao Adicionar
    And adicionado, a tarefa deve ser anexada a lista de tarefas criadas
    And o usuario deve clicar em Manage SubTasks
    And Este botao deve ter o numero de subtarefas criadas para essas tarefas
    And o usuario deve incluir uma descricao com o tamanho de <NumDescricao>
    When tambem deve incluir uma data de vencimento "MM/dd/yyyy"
    Then As subtarefas com tamanho superior ao limite que foram adicionadas devem ser anexadas na parte inferior do dialogo

Examples:
 |caracteres| 	NumDescricao	  |
 |		50	  | 			 251				|
 
Scenario Outline: Reproduzir de forma automatica o Bug5809
		Given acesso o Menu "My Tasks"
    And a tarefa deve possuir <caracteres>
    When o usuario deve poder inserir uma nova tarefa pressionando Enter
    Then adicionado, a tarefa deve ser anexada a lista de tarefas criadas

Examples:
 |caracteres|
 |		2		  |
 |		251 	|