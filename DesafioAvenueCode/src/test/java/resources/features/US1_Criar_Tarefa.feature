
#https://cucumber.io/docs/gherkin/reference/

@funcional
Feature: US1 - Criar tarefa

		Como usuario do App ToDo
		Eu deveria ser capaz de criar uma tarefa
		Entao eu posso gerenciar minhas tarefas

		Criterios de aceitacao:

				O usuario sempre deve ver o link "My Tasks" na NavBar;
				Clicar nesse link redirecionara o usuario para uma paina com todas as tarefas criadas ate o momento;
				O usuario deve ver uma mensagem na parte superior dizendo que a lista pertence ao registro do utilizador;
					Ex .: se o nome do usuario registrado for John, a mensagem exibida devera ser "Hey John, this is your todo list for today";
				O usuario deve poder inserir uma nova tarefa pressionando Enter ou clicando na tarefa do botao Adicionar;
				A tarefa deve exigir pelo menos tres caracteres para que o usuario possa inseri la.
				A tarefa nao pode ter mais de 250 caracteres
				Quando adicionado, a tarefa deve ser anexada a lista de tarefas criadas.


Scenario: 
    Given acesso o Menu "My Tasks"
    And o sistema deve exibir a seguinte mensagem ", this is your todo list for today"

Scenario Outline: Incluir Task atraves da tecla Enter
		Given acesso o Menu "My Tasks"
    And a tarefa deve possuir <caracteres>
    When o usuario deve poder inserir uma nova tarefa pressionando Enter
    Then adicionado, a tarefa deve ser anexada a lista de tarefas criadas

Examples:

 |caracteres|
 |		2		  |
 |		3 		|
 |		250 	|
 |		251 	|
 
Scenario Outline: Incluir Task atraves do botao Adicionar
    Given acesso o Menu "My Tasks"
    And a tarefa deve possuir <caracteres>
    When o usuario deve poder inserir uma nova tarefa acionando o botao Adicionar
    Then adicionado, a tarefa deve ser anexada a lista de tarefas criadas

Examples:

 |caracteres|
 |		2 		|
 |		3 		|
 |		250 	|
 |		251 	|