package utils;

import static core.DriverFactory.getDriver;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class Utils {

	public static int randomInt(int qtd) {

		Random gerador = new Random();
		int num = 0;

		for (int i = 0; i < qtd; i++) {
			num = num + (gerador.nextInt());
		}

		return num;
	}

	public static void wait(int timeSeconds) {

		try {
			Thread.sleep(timeSeconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static String createScreenshot(String testName) throws IOException {

		// Gerando ScreenShot ao final dos testes

		TakesScreenshot ss = (TakesScreenshot) getDriver();
		File arquivo = ss.getScreenshotAs(OutputType.FILE);
		String path = System.getProperty("user.dir") + File.separator + "target" + File.separator + "screenshot"
				+ File.separator + testName + " " + timestamp() + ".jpg";
		File destination = new File(path);

		try {
			FileUtils.copyFile(arquivo, destination);
		} catch (IOException e) {
			System.out.println("Capture Failed " + e.getMessage());
		}

		return path;
	}

	private static Random rand = new Random();

	private static char[] letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

	public static String stringAleatoria(int nCaracter) {

		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < nCaracter; i++) {
			int ch = rand.nextInt(letras.length);
			sb.append(letras[ch]);
		}

		return sb.toString();
	}

	public static void clickTeclaEnter(){

		Robot robot;
		try {
			robot = new Robot();
			robot.setAutoDelay(1500);

			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static void ctrlV(){

		Robot robot;
		try {
			robot = new Robot();
			robot.setAutoDelay(1500);

			robot.keyPress(KeyEvent.VK_CONTROL);
			wait(1);
			robot.keyPress(KeyEvent.VK_V);

			wait(1);

			robot.keyRelease(KeyEvent.VK_V);
			wait(1);
			robot.keyRelease(KeyEvent.VK_CONTROL);
		} catch (AWTException e) {
			e.printStackTrace();
		}
		
	}
	
	public static String AddFileWindow(String file) {

		String path = System.getProperty("user.dir");
		String pathFile = path + File.separator + "files" + File.separator + file;
		StringSelection ss = new StringSelection(pathFile);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

		return pathFile;
	}

	public static String timestamp() {

		return new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(new Date());

	}

	public static String getDataAtual(String formatDate) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(formatDate);
		LocalDate localDate = LocalDate.now();
		return (dtf.format(localDate));
	}

	public static String obterDataDiferencaDias(int dias) {

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, dias);
		
		Date date = cal.getTime();
		
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dataFormatada = dateFormat.format(date);
		
		return dataFormatada;
	}
	
	public static String dataAtualDiaSemana() {

		Calendar cal = Calendar.getInstance();
		return new DateFormatSymbols().getWeekdays()[cal.get(Calendar.DAY_OF_WEEK)];

	}
	
	public static List<String> lerArquivoTxt(String fileTxt, String pasta) {

		String path = System.getProperty("user.dir");
		String pathFile = path + File.separator + pasta + File.separator + fileTxt;

		List<String> listaString = new ArrayList<String>();
		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader(pathFile));

			for (String linha = br.readLine(); linha != null; linha = br.readLine()) {

				listaString.add(linha);

			}
		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		System.out.println(listaString);

		return listaString;

	}

}