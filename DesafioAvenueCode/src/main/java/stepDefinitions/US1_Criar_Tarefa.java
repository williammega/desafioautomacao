package stepDefinitions;

import static core.DriverFactory.getDriver;
import static org.testng.Assert.assertEquals;

import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import page.TarefasPage;
import page.TelaInicialPage;
import utils.Utils;

public class US1_Criar_Tarefa {
	
	TelaInicialPage telaInicialPage = new TelaInicialPage();
	TarefasPage tarefasPage = new TarefasPage();
	
	String descricaoTask, Nome;
	
	public WebDriverWait wait = new WebDriverWait(getDriver(), 10000);
	
	@Given("^acesso o Menu \"([^\"]*)\"$")
	public void acessoOMenuConteudo(String menu) {

		telaInicialPage.selecionarMenuMyTasks(menu);
	}
	
	@Given("^o sistema deve exibir a seguinte mensagem \"([^\"]*)\"$")
	public void oSistemaDeveExibirASeguinteMensagem(String mensagem) {
		Nome = tarefasPage.obterNome();
		Nome = "Hey " + Nome;
		mensagem = Nome + mensagem;
		
		String txtMsg = tarefasPage.obterMensagemTasks();
		assertEquals(txtMsg, mensagem, "Resultado sobre a mensagem da Tasks: ");
	}
	
	@Given("^a tarefa deve possuir (\\d+)$")
	public void aTarefaDevePossuir(int numCaracteres) throws Throwable {
		descricaoTask = Utils.stringAleatoria(numCaracteres);
		tarefasPage.setDescricaoTasks(descricaoTask);
	}

	@Given("^o usuario deve poder inserir uma nova tarefa pressionando Enter$")
	public void oUsuarioDevePoderInserirUmaNovaTarefaPressionandoEnter() {
		Utils.clickTeclaEnter();
	}

	@Given("^o usuario deve poder inserir uma nova tarefa acionando o botao Adicionar$")
	public void oUsuarioDevePoderInserirUmaNovaTarefaAcionandoOBotaoAdicionar() {
		tarefasPage.clickBotaoAdicionarTasks();
	}

	@When("^adicionado, a tarefa deve ser anexada a lista de tarefas criadas$")
	public void adicionadoATarefaDeveSerAnexadaAListaDeTarefasCriadas() {
		String txt = tarefasPage.obterConteudoGrid(1,2);
		
		//Validando regra de negocio sobre tamanho da descricao da Tasks
		//Minimo 3 - Maximo 250
		
		int tamanho = txt.length();
		
		if ((tamanho >2) && (tamanho <251)) {
			assertEquals(descricaoTask, txt, "Resultado da comparacao na GRID: ");
		}else{
			assertEquals(tamanho, 250, "Resultado do tamanho da descricao: ");			
		}
		
		//Excluir Tasks incluida
		
		tarefasPage.clickBotaoGrid(1, 5, 1);
	}
	
}