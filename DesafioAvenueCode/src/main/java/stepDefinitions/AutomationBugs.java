package stepDefinitions;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import core.BasePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import page.SubTarefaPage;
import page.TarefasPage;
import page.TelaInicialPage;
import utils.Utils;

public class AutomationBugs extends BasePage {
	
	TelaInicialPage telaInicialPage = new TelaInicialPage();
	TarefasPage tarefasPage = new TarefasPage();
	SubTarefaPage subTarefaPage = new SubTarefaPage();
	
	String descricaoSubTask;
	
	@When("^editar a subtarefa e confirma \"([^\"]*)\"$")
	public void editarASubtarefaEConfirmarComATeclaEnter(String descricaoSubTasks) {
		subTarefaPage.clickDescricaoSubTasksGrid(descricaoSubTasks);
		subTarefaPage.clickConfirmEditSubTasks();
		Utils.wait(3);
	}

	@Then("^o sistema exibe a mensagem de erro \"([^\"]*)\"$")
	public void oSistemaExibeAMensagemDeErro(String msgError) {
		String msg = subTarefaPage.obterMensagemErroEditSubTasks();
			
		//Fechar Pop-Up
		subTarefaPage.clickBtnClose();
		
		assertTrue(msg.contains(msgError));
		
		//Excluir Tasks incluida
		tarefasPage.clickBotaoGrid(1, 5, 1);
	}
	
	@And("^o usuario deve incluir uma descricao com o tamanho de (\\d+)$")
	public void oUsuarioDeveIncluirUmaDescricaoComOTamanhoDe(int numDescricao) throws Throwable {
		descricaoSubTask = Utils.stringAleatoria(numDescricao);
		subTarefaPage.setDescricaoSubTasks(descricaoSubTask);
	}
	
	@Then("^As subtarefas com tamanho superior ao limite que foram adicionadas devem ser anexadas na parte inferior do dialogo$")
	public void asSubtarefasComTamanhoSuperiorAQueForamAdicionadasDevemSerAnexadasNaParteInferiorDoDialogo() {
		String descricaoSub = subTarefaPage.obterConteudoGridSubTasks(1, 2);
		
		//Validando regra de negocio sobre tamanho da descricao da SubTasks
		//Maximo 250
		
		int tamanho = descricaoSub.length();
		
		//Fechar Pop-Up
		subTarefaPage.clickBtnClose();
		
		//Excluir Tasks incluida
		tarefasPage.clickBotaoGrid(1, 5, 1);
		
		if (tamanho >250) {
			assertEquals(descricaoSubTask, descricaoSub, "Resultado da comparacao na GRID: ");
		}
	}
	
	@When("^As subtarefas que foram adicionadas vazias devem ser anexadas na parte inferior do dialogo$")
	public void asSubtarefasQueForamAdicionadasDevemSerAnexadasNaParteInferiorDoDialogo() {
		String descricaoSubTasks = subTarefaPage.obterConteudoGridSubTasks(1, 2);

		// Fechar Pop-Up
		subTarefaPage.clickBtnClose();
		
		if (descricaoSubTasks.equals("empty")) {
			assertEquals("empty", descricaoSubTasks,
					"Resultado da comparacao de SubTasks: ");
		} else {
			assertEquals(subTarefaPage.getDescricaoSubTasks(), descricaoSubTasks,
					"Resultado da comparacao de SubTasks: ");
		}

	}
	
	@Given("^a tarefa deve ser vazia \"([^\"]*)\"$")
	public void aTarefaDeveSerVazia(String descricao) {
		tarefasPage.setDescricaoTasks(descricao);
	}
	
	@Then("^adicionado, a tarefa com a descricao vazia deve ser anexada a lista de tarefas criadas$")
	public void adicionadoATarefaComADescricaoVaziaDeveSerAnexadaAListaDeTarefasCriadas() {
		String txt = tarefasPage.obterConteudoGrid(1,2);
		assertEquals(txt, tarefasPage.getDescricaoSubTasks(), "Resultado da comparacao na GRID: ");
	}
	
	@Given("^este pop-up deve ter um campo somente leitura com o ID da tarefa e a descricao da tarefa$")
	public void estePopUpDeveTerUmCampoSomenteLeituraComOIDDaTarefaEADescricaoDaTarefa() {
		subTarefaPage.setDescricaoTasks(Utils.stringAleatoria(20));
		subTarefaPage.clickBtnClose();
	}
}