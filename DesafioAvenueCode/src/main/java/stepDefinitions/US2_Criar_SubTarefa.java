package stepDefinitions;

import static core.DriverFactory.getDriver;
import static org.testng.Assert.assertEquals;

import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.When;
import page.SubTarefaPage;
import page.TarefasPage;
import page.TelaInicialPage;

public class US2_Criar_SubTarefa {
	
	TelaInicialPage telaInicialPage = new TelaInicialPage();
	TarefasPage tarefasPage = new TarefasPage();
	SubTarefaPage subTarefaPage = new SubTarefaPage();
	
	int NumSubTasks;
	
	public WebDriverWait wait = new WebDriverWait(getDriver(), 10000);
	
	@When("^o usuario deve clicar em Manage SubTasks$")
	public void oUsuarioDeveClicarEmManageSubTasks() {
		tarefasPage.clickBotaoGrid(1, 4, 1);
	}

	@When("^Este botao deve ter o numero de subtarefas criadas para essas tarefas$")
	public void esteBotaoDeveTerONumeroDeSubtarefasCriadasParaEssasTarefas() {
		NumSubTasks = tarefasPage.obterNumBtnManageSubTasks(1, 4, 1);
		assertEquals(NumSubTasks, 0, "Resultado da comparacao de numeros de SubTasks: ");
	}
	
	@When("^o usuario deve incluir uma descricao da SubTasks \"([^\"]*)\"$")
	public void oUsuarioDeveIncluirUmaDescricaoDaSubTasks(String descricao) throws Throwable {
		subTarefaPage.setDescricaoSubTasks(descricao);
	}

	@When("^tambem deve incluir uma data de vencimento \"([^\"]*)\"$")
	public void tambemDeveIncluirUmaDataDeVencimento(String formatDate) {
		subTarefaPage.setDate(formatDate);
		subTarefaPage.clickBtnAdd();
	}

	@When("^As subtarefas que foram adicionadas devem ser anexadas na parte inferior do dialogo$")
	public void asSubtarefasQueForamAdicionadasDevemSerAnexadasNaParteInferiorDoDialogo() {
		String descricaoSubTasks = subTarefaPage.obterConteudoGridSubTasks(1, 2);

		// Fechar Pop-Up
		subTarefaPage.clickBtnClose();
		
		if (descricaoSubTasks.equals("empty")) {
			assertEquals(subTarefaPage.getDescricaoSubTasks(), descricaoSubTasks,
					"Resultado da comparacao de SubTasks: ");
		} else {
			assertEquals(subTarefaPage.getDescricaoSubTasks(), descricaoSubTasks,
					"Resultado da comparacao de SubTasks: ");
		}

		// Validar a quantidade de subtarefas associadas a tarefa
		// Validacao sendo realizada atraves do botao "Manage SubTasks"s
		NumSubTasks = tarefasPage.obterNumBtnManageSubTasks(1, 4, 1);
		assertEquals(NumSubTasks, 1, "Resultado da comparacao de numeros de SubTasks: ");

		// Excluir Tasks incluida
		tarefasPage.clickBotaoGrid(1, 5, 1);
		
	}
}