package page;

import core.BasePage;
import utils.Utils;

public class SubTarefaPage extends BasePage {
	
	//edit_task
	
	String descricaoSub;
	
	public void setDescricaoSubTasks(String Descricao) {
		descricaoSub = Descricao;
		dsl.escrever("new_sub_task", Descricao);
	}
	
	public String getDescricaoSubTasks() {
		return descricaoSub;
	}
	
	public void setDescricaoTasks(String editDescricao) {
		dsl.escrever("edit_task", editDescricao);
	}

	public void setDate(String formatDate) {
		dsl.escrever("dueDate", Utils.getDataAtual(formatDate));
	}

	public void clickBtnAdd() {
		dsl.clicarBotao("add-subtask");
	}
	
	public void clickBtnClose() {
		dsl.clicarBotaoXpath("//button[contains(text(),'Close')]");
	}
	
	public String obterConteudoGridSubTasks(int linha, int coluna) {
		return dsl.obterValorCampoXpathConteudo("//body[1]//div[2]/table[1]/tbody[1]/tr["+linha+"]/td["+coluna+"]");
	}
	
	public void clickDescricaoSubTasksGrid(String linkDescricao) {
		dsl.clicarLink(linkDescricao);
	}	
	
	public void setEditDescricaoSubTasks(String editDescricao) {
		Utils.wait(1);
		dsl.escreverXpath("/html//tbody[1]//input[1]", editDescricao);
	}	
	
	public void clickConfirmEditSubTasks() {
		dsl.clicarBotaoXpath("//span[@class='editable-buttons']//button[@class='btn btn-primary']");
		Utils.wait(1);
	}

	public String obterMensagemErroEditSubTasks() {
		return dsl.obterValorCampoXpathConteudo("//div[@class='editable-error help-block ng-binding']");
	}	
	
	public String clickBotaoRemoveGridSubTasks(int linha) {
		return dsl.obterValorCampoXpathConteudo("//tr["+linha+"]//td[3]//button[1]");
	}	
		
}
