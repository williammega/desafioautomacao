package page;
import core.BasePage;

public class TarefasPage extends BasePage {

	String descricao;
	
	public void setDescricaoTasks(String name) {
		descricao = name;
		dsl.escreverName("new_task", name);
	}
	
	public String getDescricaoSubTasks() {
		return descricao;
	}
	
	public void clickBotaoAdicionarTasks() {
		dsl.clicarBotaoXpath("//span[@class='input-group-addon glyphicon glyphicon-plus']");
	}
	
	public String obterConteudoGrid(int linha, int coluna) {
		return dsl.obterValorCampoXpathConteudo("//tr["+linha+"]//td["+coluna+"]");
	}	
	
	public String obterNome() {
		String welcome;
		welcome = dsl.obterValorCampoXpathConteudo("//a[contains(text(),'Welcome')]");
		welcome = welcome.replace("Welcome, ", "");
		welcome = welcome.replace("!", "");
		return welcome;
	}	
	
	public String obterMensagemTasks() {
		return dsl.obterValorCampoXpathConteudo("/html[1]/body[1]/div[1]/h1[1]");
	}
	
	public void clickBotaoGrid(int linha, int coluna, int button) {
		
		/*
		 - Coluna: 4 - Manage Subtasks
		 - Coluna: 5 - Remove		 
		*/
		
		dsl.clicarBotaoXpath("//tr["+linha+"]//td["+coluna+"]//button["+button+"]");
	}
	
	public int obterNumBtnManageSubTasks(int linha, int coluna, int button) {
		String NumSubTasks;
		NumSubTasks = dsl.obterValorCampoXpathConteudo("//tr["+linha+"]//td["+coluna+"]//button["+button+"]");
		NumSubTasks = NumSubTasks.replace(") Manage Subtasks", "");
		NumSubTasks = NumSubTasks.replace("(", "");
		int result = Integer.parseInt(NumSubTasks);
		return result;
	}
}