package page;

import static core.DriverFactory.getDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import core.BasePage;
import core.DSL;

public class LoginPage extends BasePage {
	
	public void clickSign() {

		WebDriverWait wait = new WebDriverWait(getDriver(), 2000);
		dsl.clicarLink("Sign In");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("user_email")));
		
	}
	
	public void setLogin(String login) {

		dsl.escrever("user_email", login);
	}

	public void setSenha(String senha) {
		dsl.escrever("user_password", senha);
	}

	public void clickBtnEntrar() {
		dsl.clicarBotaoName("commit");
	}

	public static void realizarLogin() {
		
		System.out.println("Realizando Login");
		
		LoginPage loginpage;
		loginpage = new LoginPage();
		
		//Realizando Login
		
		loginpage.clickSign();
		loginpage.setLogin(user);
		loginpage.setSenha(pass);
		loginpage.clickBtnEntrar();
		
		//Espera
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='alert alert-info']")));
		
	}
	
	public static void realizarLogout() {
		
		DSL dsl;
		dsl = new DSL();

		//Realizar Logout

		WebDriverWait wait = new WebDriverWait(getDriver(), 2000);

		dsl.clicarLink("Sign out");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='alert alert-info']")));

	}
	
}