package core;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.*;

import core.Propriedades.TipoExecucao;

public class DriverFactory {

	protected static WebDriver driver;

	protected DriverFactory() {
	}

	public static WebDriver getDriver() {
		
		
		if(Propriedades.tipoExecucao == TipoExecucao.LOCAL){
		
				if (driver == null) {
					switch (Propriedades.browser) {
					case CHROME:
						executeChrome(BasePage.url);
						break;
					case FIREFOX:
						executeFirefox(BasePage.url);
						break;
					}
				}
		}
		
		if(Propriedades.tipoExecucao == TipoExecucao.GRID) {
			
			if (driver == null) {			
				DesiredCapabilities cap = null;
					switch (Propriedades.browser) {
					case CHROME:
						cap = DesiredCapabilities.chrome();
						break;
					case FIREFOX:
						cap = DesiredCapabilities.firefox();
						break;
					}
									
					try {
						driver = new RemoteWebDriver(new URL("http://xx.xx.x.xx:4444/wd/hub"), cap);
						((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());

					} catch (MalformedURLException e) {
						System.err.println("Falha ao conectar ao GRID");
					}
				driver.manage().window().maximize();
				driver.get(BasePage.url);
				System.out.println("Abriu Browser Grid");
			}
	}

		if (Propriedades.tipoExecucao == TipoExecucao.HEADLESS) {

			if (driver == null) {
				switch (Propriedades.browser) {
					case CHROME:
						executeChromeHeadLess(BasePage.url);
						break;
					case FIREFOX:
						executeFirefoxHeadLess(BasePage.url);
						break;
				}

				driver.manage().window().maximize();
			}

		}

		//https://wiki.saucelabs.com/display/DOCS/Platform+Configurator#/
		//https://app.saucelabs.com/dashboard/onboarding
		//Automated Tests
		//https://app.saucelabs.com/dashboard/tests
		//https://saucelabs.com/blog/selenium-tips-uploading-files-in-remote-webdriver

		return driver;
}
	public static void executeChrome(String urlSystem){

		String driverpathChrome = System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", driverpathChrome);
		driver = new ChromeDriver();
		getDriver().get(urlSystem);
		driver.manage().window().maximize();
	}

	public static void executeChromeHeadLess(String urlSystem){

		String driverpathChrome = System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", driverpathChrome);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		driver = new ChromeDriver(options);
		getDriver().get(urlSystem);
		driver.manage().window().maximize();
	}
	
	public static void executeFirefox(String urlSystem){

		String driverpathFirefox = System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "geckodriver.exe";
		System.setProperty("webdriver.gecko.driver", driverpathFirefox);
		driver = new FirefoxDriver();
		getDriver().get(urlSystem);
		driver.manage().window().maximize();
	}

	public static void executeFirefoxHeadLess(String urlSystem){

		String driverpathFirefox = System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "geckodriver.exe";
		System.setProperty("webdriver.gecko.driver", driverpathFirefox);
		FirefoxOptions options = new FirefoxOptions();
		options.addArguments("--headless");
		driver = new FirefoxDriver(options);
		getDriver().get(urlSystem);
		driver.manage().window().maximize();
	}
	
	public static void killDriver() {
		if (driver != null) {
			driver.quit();			
			driver = null;
		}
	}
}
