package core;

import java.util.List;

public class BasePage {

	protected static DSL dsl;
	
	static List<String> result = Ambiente.dadosAmbiente();
	static String url = result.get(0);
	protected static String user = result.get(1);
	protected static String pass = result.get(2);

	public BasePage() {
		dsl = new DSL();
	}

}