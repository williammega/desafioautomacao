package core;

import java.io.File;
import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import org.apache.commons.lang3.reflect.FieldUtils;
import cucumber.runtime.ScenarioImpl;
import gherkin.formatter.model.Result;
import java.lang.reflect.Field;
import java.util.ArrayList;

import cucumber.api.Scenario;
import utils.Utils;

public class ExtentReport {

	protected static ExtentReports extent;
	protected static ExtentTest logger;
	public static String diretorioReport = "";

	public static ExtentReports StartReport(String testName) {

		String path = System.getProperty("user.dir");

		// Para criar o relatorio somente uma vez
		
		if (extent == null) {

			diretorioReport = path + File.separator + "report" + File.separator + "ExtentReport_Automation_AvenueCode_"
					+ Utils.timestamp() + ".html";
			ExtentHtmlReporter reporter = new ExtentHtmlReporter(diretorioReport);

			reporter.config().setTimeStampFormat("dd/MM/yyyy HH:mm:ss");
			reporter.config().setEncoding("UTF-8");
			
			reporter.config().setDocumentTitle("Dashboard - Automation");
			reporter.config().setReportName("Summary Test");
			reporter.config().setTheme(Theme.STANDARD);

			extent = new ExtentReports();
			extent.attachReporter(reporter);
		}

		logger = extent.createTest(testName);
		
		return extent;
	}

	public static void ResultTest(Scenario cenario) throws IOException {

		String testName = cenario.getName();

		if (cenario.isFailed()) {
				
			logger.fail("Evidencia - Teste com Falha: ",
					MediaEntityBuilder.createScreenCaptureFromPath(Utils.createScreenshot(testName)).build());
				
			logError(cenario);
		}

		if (cenario.getStatus() == "passed") {

			logger.pass("Teste com Sucesso.");
		}

		if (cenario.getStatus() == "skipped") {

			logger.skip("Teste ignorado");

		}
	}

	@SuppressWarnings("unchecked")
	private static void logError(Scenario cenario) {
		
		Field field = FieldUtils.getField(((ScenarioImpl) cenario).getClass(), "stepResults", true);
		field.setAccessible(true);
		
		try {
			
			ArrayList<Result> results = (ArrayList<Result>) field.get(cenario);			
			
			for (Result result : results) {
				
					if (result.getError() != null)
						
						logger.fail(result.getError());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
}
